# CES QR

[es]

Generamos un QR que permite agilizar las transacciones.

Una vez escaneado el QR de la cuenta a la que se desea cobrar, la url
generada te envia directamente al formulario evitando tener que elegir
la ecored y la cuenta.

## Pendiente

Poder escanear el QR directamente desde la aplicación.

Una buena opción podría ser https://github.com/dwa012/html5-qrcode

[en]
