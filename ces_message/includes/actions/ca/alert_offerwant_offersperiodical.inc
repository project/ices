<?php

/**
 * @file
 * Catalan alert periodical offers template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Llistat de les darreres ofertes';

$body = 'Benvolgut/da,

Aquesta és una llista de les darreres actualitzacions d\'ofertes a la teva xarxa.

';
