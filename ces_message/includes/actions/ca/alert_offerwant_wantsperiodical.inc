<?php

/**
 * @file
 * Catalan alert periodical wants template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Llistat de les darreres demandes';

$body = 'Benvolgut/da,

Aquesta és una llista de les darreres actualitzacions de demandes a la teva xarxa.

';
