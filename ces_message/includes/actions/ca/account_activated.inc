<?php

/**
 * @file
 * English Account activated template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Compte activat';

$body = 'Estimat/da [account:user:fullname],

Benvingut/da a [exchange:name] ([exchange:code]), la teva xarxa local d\'intercanvis dins la plataforma Integral Community Exchange System.

A continuació hi ha els detalls per accedir al teu compte a la xarxa [exchange:code].

Per anar al lloc web segueix l\'enllaç [site:url]. Et portarà a la pàgina d\'entrada.

Entra el teu nom d\'usuari i password:

Nom d\'usuari: [account:user:name]
Password: *

* La contrasenya. Si encara no tens una, rebràs un altre email amb l\'enllaç per generar-la.

La interfície de la web està traduïda a varis idiomes. Si vols seleccionar un altre idioma, clica el teu nom a la cantonada de dalt a la dreta. En el teu perfil personal, selecciona el teu idioma preferit. Assegura\'t també de que les teves dades personals són correctes i completes.

Després de fer una ullada per programa, entra les teves ofertes i demandes clicant als enllaços Les meves ofertes i Les meves demandes del menú de l\'esquerra. Si ja n\'has entrat durant el registre, pots modificar-les i afegir-ne tantes com desitgis.

També pots pujar una fotografia teva per a que la resta poguem veure qui ets.

No dubtis en contactar els anunciants de la llista d\'ofertes per adquirir productes i serveis. No necessites esperar que et comprin a tu per a poder utilitzar el sistema. Tenir el compte en negatiu és habitual i part del sistema i simplement significa el teu compromís amb la comunitat.

Si necessites ajuda contacta l\'administrador/a de la xarxa.

Feliç intercanvi!

--
Administrador/a de [exchange:shortname]:
[exchange:admin:fullname]
[exchange:admin:mail]
[exchange:admin:mainphone]
';
