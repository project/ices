<?php

/**
 * @file
 * Catalan alert new want template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Nova demanda a la teva xarxa';

$body = 'Benvolgut/da,

Una nova demanda s\'ha creat a la teva xarxa.

';
