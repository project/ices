<?php

/**
 * @file
 * English send offerwant notify template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Missatge sobre [offer:title]';

$body = '[notify:body]

Usuari/a: [notify:user:fullname] < [notify:user:mail] >
';
