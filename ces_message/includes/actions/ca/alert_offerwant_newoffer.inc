<?php

/**
 * @file
 * Catalan alert new offer template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Nova oferta a la teva xarxa';

$body = 'Benvolgut/da,

Una nova oferta s\'ha creat a la teva xarxa.

';
