<?php

/**
 * @file
 * English alert new offer template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] New offer in your network';

$body = 'Dear user,

A new offer has been created in your network.

';
