<?php

/**
 * @file
 * English alert periodical wants template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Last wants list';

$body = 'Dear user,

This is a list of the last updated wants in your network.

';
