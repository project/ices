<?php

/**
 * @file
 * English send offerwant notify template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] New message in [offer:title]';

$body = '[notify:body]

User: [notify:user:fullname] < [notify:user:mail] >
';
