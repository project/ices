<?php

/**
 * @file
 * English exchange activated template.
 *
 * @ingroup ces_message_template
 */

$subject = '[Integral CES] Exchange [exchange:code] - [exchange:shortname] activated';

$body = 'Dear [exchange:admin:name],

The new [exchange:name] has been activated. So far we have only created the administrator\'s account, [exchange:code]0000. Don\'t use this account for personal operations. Create a new user and a new account for your personal activity within the exchange network.

As there is great demand for this web service we have to limit the period that we can give you to get this new exchange operational. If there is no trading activity within six months of today then unfortunately we will have to take the exchange down to make space for others.

This is only the beginning. You have a great work making this exchange network a really useful tool.

Please confirm that you have received this welcome letter.

--
IntegralCES
[site:mail]
';
