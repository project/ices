<?php

/**
 * @file
 * English alert new want template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] New want in your network';

$body = 'Dear user,

A new want has been created in your network.

';
