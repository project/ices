<?php

/**
 * @file
 * English alert periodical offers template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Last offerings list';

$body = 'Dear user,

This is a list of the last updated offerings in your network.

';
