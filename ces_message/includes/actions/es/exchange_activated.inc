<?php

/**
 * @file
 * English exchange activated template.
 *
 * @ingroup ces_message_template
 */

$subject = '[Integral CES] Exchange [exchange:code] - [exchange:shortname] activada';

$body = 'Estimado/a [exchange:admin:name],

La nueva red local de intercamvios [exchange:name] ha sido activada. Hasta ahora sólo hemos creado el administrador de la cuenta [exchange:code]0000.

No utilices esta cuenta para las operaciones personales. Crea un nuevo usuario y una nueva cuenta para tu actividad personal dentro de la red de intercambio.

Como existe una gran demanda de este servicio web tenemos que limitar el período en el que tenemos operativa la red local. Si no hay actividad comercial dentro de los seis meses a partir de hoy, entonces, lamentablemente tendremos que cerrar la red para hacer espacio para otros.

Este es sólo el comienzo. Tiene un gran trabajo haciendo de esta red de intercambio una herramienta muy útil.

Por favor, confirma que ha recibido esta carta de bienvenida.

--
IntegralCES
[site:mail]
';
