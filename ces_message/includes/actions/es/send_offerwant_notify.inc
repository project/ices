<?php

/**
 * @file
 * English send offerwant notify template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Mensaje sobre [offer:title]';

$body = '[notify:body]

Usuario/a: [notify:user:fullname] < [notify:user:mail] >
';
