<?php

/**
 * @file
 * Spanish alert new offer template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Nueva oferta en tu red';

$body = 'Estimado/a,

Una nueva oferta se ha creado en tu red.

';
