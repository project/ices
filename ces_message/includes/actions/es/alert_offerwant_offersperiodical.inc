<?php

/**
 * @file
 * Spanish alert periodical offers template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Listado de últimas ofertas';

$body = 'Estimado/a,

Este es un listado de las últimas actualizaciones de ofertas en tu red.

';
