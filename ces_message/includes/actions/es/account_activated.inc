<?php

/**
 * @file
 * English Account activated template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Cuenta activada';

$body = 'Estimado/a [account:user:fullname],

Bienvenido/a a [exchange:name] ([exchange:code]), tu red local de intercambios dentro de la plataforma Integral Community Exchange System.

A continuación encontrarás los detalles para acceder a tu cuenta en la red [exchange:code].

Para ir al sitio web sigue el enlace [site:url]. Te llevará a la página de entrada.

Entra tu nombre de usuario/a y contraseña:

Nombre de usuario/a: [account:user:name]
Contraseña: *

* Tu contraseña. Si aun no tienes una, recibiras otro email con el enlace para generarla. 

La interfaz de la web está traducida a varias lenguas. Si quieres seleccionar otro idioma, clica en tu nombre en la esquina superior derecha. En tu perfil personal, selecciona tu idioma preferido. Assegurate también de que tus datos son correctos y completos.

Tras echar un vistazo por el programa, entra tus ofertas i demandas clicando los enlaces Mis ofertas y Mis demandas del menú de la izquierda. Si ya entrastes ofertas durante el registro, puedes modificarlas y añadir tantas como quieras.

Tambien puedes subir una fotografia tuya para que el resto podamos ver quien eres.

No dudes en contactar los anunciantes de la lista de ofertas para adquirir productos y servicios. No necessitas esperar a que te comprien para poder utilizar el sistema. Tener la cuenta en negativo és habitual y parte del sistema y simplemente significa tu compromiso con la comunidad.

Si necessitas ayuda contacta el/la administrador/a de la red.

Feliz intercambio!

--
Administrador/a de [exchange:shortname]:
[exchange:admin:fullname]
[exchange:admin:mail]
[exchange:admin:mainphone]
';
