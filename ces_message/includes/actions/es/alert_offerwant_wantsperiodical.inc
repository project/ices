<?php

/**
 * @file
 * Spanish alert periodical wants template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Listado de últimas demandas';

$body = 'Estimado/a,

Este es un listado de las últimas actualizaciones de demandas en tu red.

';
