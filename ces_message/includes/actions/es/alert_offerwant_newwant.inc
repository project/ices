<?php

/**
 * @file
 * Spanish alert new want template.
 *
 * @ingroup ces_message_template
 */

$subject = '[[exchange:shortname]] Nueva demanda en tu red';

$body = 'Estimado/a,

Una nueva demanda ha sido creada en tu red.

';
