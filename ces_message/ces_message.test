<?php
/**
 * @file
 * Tests from Ces Message
 */

/**
 * @defgroup ces_message_tests Tests from ces_message
 * @ingroup ces_message
 * @{
 * Tests the functionality Ces Message module.
 */

/**
 * Implements DrupalWebTestCase.
 */
class CesMessageTestCase extends DrupalWebTestCase {

  /**
   * Bank.
   */
  protected $bank;
  /**
   * The global administrator.
   */
  protected $globalAdminUser;
  /** 
   * Exchanges demo. 
   */
  protected $exchangesDemo;
  /**
   * Offers demo.
   */
  protected $offersDemo;
  /**
   * Implementation of getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'ICES Message',
      'description' => 'Test ICES Message.',
      'group' => 'ICES',
    );
  }

  /**
   * Setup CES modules, the admin user, configures blocks and user preferences.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp(array(
      'ces_bank',
      'ces_blog',
      'ces_message',
      'ces_offerswants',
      'ces_summaryblock',
      'ces_develop',
      'ces_user',
    ));
    include dirname(__FILE__) . '/../ces_develop/demo.php';
    $this->offersDemo = $offers_demo;
    $this->exchangesDemo = $exchanges_demo;
    $bank = new CesBank();
    $this->globalAdminUser = $this->drupalCreateUser(array(
      'administer blocks',
      'access administration pages',
      'administer users',
      'access user profiles',
    ));
    $this->drupalLogin($this->globalAdminUser);
    $edit = array();
    $edit['user_email_verification'] = FALSE;
    $this->drupalPost('admin/config/people/accounts', $edit,
      t('Save configuration'));
    $permission = array(
      'permission' => CesBankPermission::PERMISSION_ADMIN,
      'object' => 'global',
      'objectid' => 0,
      'scope' => 'user',
      'scopeid' => $this->globalAdminUser->uid,
    );
    $bank->createPermission($permission);
    $this->drupalLogout();
  }
  /**
   * Ces Message.
   */
  protected function testCesMessage() {
    $this->drupalLogin($this->globalAdminUser);
    // $edit = array(
    // 'name' => 'Riemann',
    // 'pass' => 'integralces',
    // );
    // $this->drupalPost('user/', $edit, 'Log in');
    $actions_page1 = array(
      'new_account' => 'New account',
      'account_activated' => 'Account activated',
      'account_debited' => 'Account debited',
      'account_credited' => 'Account credited',
      'send_offerwant_notify' => 'Send offer or want notify',
      // @todo 'account_credit_pending' => 'Account credit pending',
      // @todo 'account_debit_pending' => 'Account debit pending',
      'account_debit_rejected' => 'Account debit rejected',
      'account_credit_rejected' => 'Account credit rejected',
    );
    $actions_page2 = array(
      'exchange_activated' => 'Exchange activated',
    );
    $actions_page3 = array(
      'new_exchange' => 'New exchange',
    );
    // Send message to author of offer.
    $notify = array('body' => 'Test body notification', 'user' => 2);
    ces_message_send_notify(
      'send_offerwant_notify',
      array(
        'exchange' => $this->exchangesDemo[0],
        'offer' => $this->offersDemo[0],
        'notify' => $notify,
      ),
      array(2),
      1
    );

    $this->drupalGet('ces/bank/account/message/all');
    foreach ($actions_page1 as $action => $tit_action) {
      $this->assertRaw($tit_action);
    }
    $this->drupalGet('ces/bank/account/message/all', array('query' => array('page' => 1)));
    foreach ($actions_page2 as $action => $tit_action) {
      $this->assertRaw($tit_action);
    }
    $this->drupalGet('ces/bank/account/message/all', array('query' => array('page' => 2)));
    foreach ($actions_page3 as $action => $tit_action) {
      $this->assertRaw($tit_action);
    }
    $this->drupalLogout();
  }
  /**
   * Test Ces Message features.
   *
   * - CRUD
   * - Table display
   * - User access
   * - Field management
   * - Display management
   */
  public function testCesMessageTemplate() {
    $this->drupalLogin($this->globalAdminUser);
    // Create 5 templates.
    for ($i = 1; $i <= 5; $i++) {
      $edit[$i]['subject'] = $this->randomName();
      $edit[$i]['body'] = 'Body ' . $this->randomName();
      $edit[$i]['ces_action'] = $i;
      $edit[$i]['language'] = 'xx';

      $this->drupalPost('ces/admin/message/add', $edit[$i], 'Save');

      $message = t('Inserted template');
      $this->assertText($message);

      // What id?
      // $this->drupalGet('ces/admin/message/' . $i);
      // $this->assertText($edit[$i]['body']);
      // $this->assertText($edit[$i]['ces_message_test_text[und][0][value]']);
    }

    // Delete entity 4.
    $this->drupalGet('ces/admin/message/templates');
    $this->clickLink('xx');
    $this->clickLink(t('Edit'));
    // $this->clickLink(t('Delete'));
    // $this->assertResponse(404, 'Deleted entity 5 no longer exists');
    unset($edit[4]);

    // Update entity 2 and verify the update.
    // $edit[2] = array(
    // 'subject' => 'updated entity 2 ',
    // 'ces_message_test_text[und][0][value]' => 'updated entity 2 test text',
    // );
    // $this->drupalPost('ces/admin/message/2/edit', $edit[2], 'Save');
    // $this->assertText('updated entity 2 test text');
    //
    // View the entity list page  and verify that the items which still exist
    // are there, and that the deleted #5 no longer is there.
    $this->drupalGet('ces/admin/message');
    foreach ($edit as $id => $item) {
      $this->assertText($item['subject']);
    }
    $this->assertRaw('ces/admin/message/3');

    // Add a field through the field UI and verify that it behaves correctly.
    // $field_edit = array(
    // 'fields[_add_new_field][label]' => 'New junk field',
    // 'fields[_add_new_field][field_name]' => 'new_junk_field',
    // 'fields[_add_new_field][type]' => 'text',
    // 'fields[_add_new_field][widget_type]' => 'text_textfield',
    // );
    // $this->drupalPost('admin/structure/ces_message_template/manage/fields',
    // $field_edit, t('Save'));
    // $this->drupalPost(NULL, array(), t('Save field settings'));
    // $this->drupalPost(NULL, array(), t('Save settings'));
    // $this->assertResponse(200);
    //
    // // Now verify that we can edit and view this entity with fields.
    // $edit[10]['field_new_junk_field[und][0][value]'] = $this->randomName();
    // $this->drupalPost('ces/admin/message/10/edit', $edit[10], 'Save');
    // $this->assertResponse(200);
    // $this->assertText($edit[10]['field_new_junk_field[und][0][value]'],
    // 'Custom field updated successfully');

    $this->drupalLogout();

    // Create and login user without view access.
    $account = $this->drupalCreateUser(array('access content'));
    $this->drupalLogin($account);
    $this->drupalGet('ces/admin/message');
    $this->assertResponse(403);
    $this->drupalGet('ces/admin/message/2');
    $this->assertResponse(403, 'User does not have permission to view entity');
  }

}
/** @} */
