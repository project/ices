<?php
/**
 * @file
 * Tests the functionality bank module.
 */

/**
 * @defgroup ces_bank_test Tests from Ces Bank
 * @ingroup ces_bank
 * @{
 * Tests the functionality bank module.
 */

/**
 * Implements DrupalWebTestCase.
 */
class CesBankTestCase extends DrupalWebTestCase {
  /**
   * Global Admin User.
   */
  protected $globalAdminUser;
  /**
   * First exchange properties.
   */
  protected $exchange1;
  /**
   * Second exchange propeties.
   */
  protected $exchange2;
  /**
   * Administrator user for exchange 1.
   */
  protected $admin1;
  /**
   * First account from exchange 1.
   */
  protected $account11;
  /**
   * Second account from exchange 1.
   */
  protected $account12;
  /**
   * Virtual account from exchange 1 to 2.
   */
  protected $accountV1;
  /**
   * Virtual account from exchange 2 to 1.
   */
  protected $accountV2;
  /**
   * Administrator user for exchange 2.
   */
  protected $admin2;
  /**
   * First account from exchange 2.
   */
  protected $account21;

  /**
   * Implementation of getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'ICES bank',
      'description' => 'Test ICES Bank main features.',
      'group' => 'ICES',
    );
  }
  /**
   * Return the modules to be enabled. Useful for subclasses.
   */
  protected function getNeededModules() {
    return array(
      'ces_bank',
      'ces_blog',
      'ces_message',
      'ces_offerswants',
      'ces_summaryblock',
      'ces_user',
    );
  }
  /**
   * Setup CES modules, admin user, configures blocks and user preferences.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp($this->getNeededModules());
    // Create global bank administrator.
    $this->globalAdminUser = $this->drupalCreateUser(array(
      'administer blocks',
      'access administration pages',
      'administer users',
      'administer site configuration',
    ));
    $this->drupalLogin($this->globalAdminUser);
    // Setup blocks.
    $edit = array();
    $edit['blocks[ces_user_ces_user_links][region]'] = 'footer';
    $edit['blocks[ces_user_ces_user_register][region]'] = 'sidebar_first';
    $this->drupalPost('admin/structure/block', $edit, t('Save blocks'));
    // Setup users.
    $edit = array();
    $edit['user_email_verification'] = FALSE;
    $this->drupalPost('admin/config/people/accounts', $edit, t('Save configuration'));
    $permission = array(
      'permission' => CesBankPermission::PERMISSION_ADMIN,
      'object' => 'global',
      'objectid' => 0,
      'scope' => 'user',
      'scopeid' => $this->globalAdminUser->uid,
    );
    $bank = new CesBank();
    $bank->createPermission($permission);
    $this->drupalLogout();
  }
  /**
   * This function performs all tests.
   *
   * Since we have to perform heavy operations (create and activate exchanges,
   * accounts, etc) before we can actually test critical features we join all in
   * one test function to save resources.
   */
  protected function testAll() {
    $this->createExchanges();
    $this->createAccounts();
    $this->doTransactions();
    // Balance: HORA0001: 1.5, HORA0002: -1.5.
    $this->checkLimits();
    // Balance: HORA0001: 19.5, HORA0002: -19.5.
    $this->checkManualAuthorization();
    // Balance: HORA0001: 10.25, HORA0002: -10.25.
    $this->doTransactionsInterexchange();
    $this->adminTransactions();
    // Balance: HORA0001: 10.75, BCNA0001: -5.
    $this->doAdminTransaction();
    // Balance: HORA0001: 11.25, HORA0002: -10.75.
    $this->checkInterexchangeManualAccept();
    // Balance: HORA0001: 11.15, BCNA0001: -4.
    $this->checkVisibilityOptions();
    // Other accounts page.
    $this->checkOtherAccountsPage();
    $this->doEditTransactions();
    // Balance: HORA0001: 11.40, HORA0002: -11.00.
    $this->doEditInterexchangeTransactions();
    // Balance: HORA0001: 12.40, BCNA0001: -14.00.
    $this->doRemoveInterexchangeTransactions();
    // Balance: HORA0001: 10.90, BCNA0001: 1.00.
    $this->doRemoveTransactions();
    // Balance: HORA0001: 9.40, HORA0002: -9.50.
    $this->adminAccounts();
  }
  /**
   * Creates two exchanges.
   */
  protected function createExchanges() {
    // Create first exchange.
    $this->admin1 = (object) array(
      'name' => 'adminhora',
      'mail' => 'test.adminhora@integralces.net',
      'pass_raw' => 'integralces',
      'firstname' => 'Admin',
      'surname' => 'Hora',
      'address' => 'Plaça major',
      'town' => 'Manresa',
      'postcode' => '08241',
      'phonemobile' => '7777777',
      'phonework' => '88888888',
      'phonehome' => '99999999',
      'website' => 'http://www.integralces.net',
    );
    $this->exchange1 = array(
      'code' => 'HORA',
      'shortname' => 'EX Bages',
      'fullname' => 'A bona hora - Ecoxarxa del Bages',
      'website' => 'http://abonahora.wordpress.com',
      'country' => 'ES',
      'region' => 'Bages',
      'town' => 'Manresa',
      'map' => 'http://maps.google.com/?ll=41.723796,1.832142&spn=0.083663,0.145912&hnear=Manresa,+Province+of+Barcelona,+Catalonia,+Spain&t=m&z',
      'lat' => 41.723796,
      'lng' => 1.832142,
      'currencysymbol' => 'ℏ',
      'currencyname' => 'hora',
      'currenciesname' => 'hores',
      'currencyvalue' => '1.0',
      'currencyscale' => '2',
      'registration_offers' => '1',
      'registration_wants' => '1',
    );
    $this->createExchange(array_merge($this->exchange1, $this->accountRecord((array) $this->admin1)));
    $this->activateExchange($this->exchange1);
    // Create second exchange.
    $this->admin2 = (object) array(
      'name' => 'adminbcna',
      'mail' => 'test.adminbcna@integralces.net',
      'pass_raw' => 'integralces',
      'firstname' => 'Admin',
      'surname' => 'Barcelona',
      'address' => 'Plaça Catalunya',
      'town' => 'Barcelona',
      'postcode' => '08000',
      'phonemobile' => '11111111',
      'phonework' => '22222222',
      'phonehome' => '33333333',
      'website' => 'http://www.integralces.net',
    );
    $this->exchange2 = array(
      'code' => 'BCNA',
      'shortname' => 'EX Barna',
      'fullname' => 'Ecoxarxa de Barcelona',
      'website' => 'http://cooperativa.ecoxarxes.cat',
      'country' => 'ES',
      'region' => 'Barcelonès',
      'town' => 'Barcelona',
      'map' => 'http://maps.google.com/barcelona',
      'lat' => 41.323796,
      'lng' => 2.132142,
      'currencysymbol' => 'ECO',
      'currencyname' => 'eco',
      'currenciesname' => 'ecos',
      'currencyvalue' => '0.1',
      'currencyscale' => '2',
      'registration_offers' => '0',
      'registration_wants' => '0',
    );
    $this->createExchange(array_merge($this->exchange2, $this->accountRecord((array) $this->admin2)));
    $this->activateExchange($this->exchange2);
  }
  /**
   * Creates three accounts.
   */
  protected function createAccounts() {
    $this->account11 = array(
      'exchange' => $this->exchange1,
      'account' => array(
        'name' => 'account11',
        'mail' => 'account11@integralces.net',
        'pass_raw' => 'integralces',
        'firstname' => 'John',
        'surname' => 'Smith',
        'address' => 'Main street 68',
        'town' => 'Manresa',
        'postcode' => '08241',
        'phonemobile' => '123456789',
        'phonework' => '987654321',
        'phonehome' => '135792468',
        'website' => 'http://www.integralces.net',
      ),
      'bankaccount' => array(
        'kind' => '0',
      ),
      'offer' => array(
        array(
          'title' => 'First test offer',
          'body' => 'This is the first test offer from John Smith account.',
          'rate' => '0.35',
          'keywords' => 'test',
        ),
      ),
      'want' => array(
        array(
          'title' => 'First test want',
          'body' => 'This is the first test want from John Smith account.',
          'keywords' => 'test',
        ),
      ),
    );
    $this->createAccount($this->account11);
    $this->activateAccount($this->admin1, 'HORA0001');
    $this->account12 = array(
      'exchange' => $this->exchange1,
      'account' => array(
        'name' => 'account12',
        'mail' => 'account12@integralces.net',
        'pass_raw' => 'integralces',
        'firstname' => 'Ofelia',
        'surname' => 'Tia',
        'address' => 'Gran vía 25',
        'town' => 'St. vicenç de castellet',
        'postcode' => '08321',
        'phonemobile' => '11111111',
        'phonework' => '22222222',
        'phonehome' => '',
        'website' => 'http://www.integralces.net',
      ),
      'bankaccount' => array(
        'kind' => '0',
      ),
      'offer' => array(
        array(
          'title' => 'Second test offer',
          'body' => 'This is the second test offer from Ofelia Tia account.',
          'rate' => 'contact me',
          'keywords' => 'test',
        ),
      ),
      'want' => array(
        array(
          'title' => 'Second test want',
          'body' => 'This is the second test want from Ofelia Tia account.',
          'keywords' => 'test',
        ),
      ),
    );
    $this->createAccount($this->account12);
    $this->activateAccount($this->admin1, 'HORA0002');
    $this->account21 = array(
      'exchange' => $this->exchange2,
      'account' => array(
        'name' => 'account21',
        'mail' => 'account21@integralces.net',
        'pass_raw' => 'integralces',
        'firstname' => 'Emmy',
        'surname' => 'Noether',
        'address' => 'Gran vía 25',
        'town' => 'Barcelona',
        'postcode' => '08015',
        'phonemobile' => '333333333',
        'phonework' => '444444444',
        'phonehome' => '',
        'website' => 'https://integralces.net',
      ),
      'bankaccount' => array(
        'kind' => '0',
      ),
      'offer' => array(),
      'want' => array(),
    );
    $this->createAccount($this->account21);
    $this->activateAccount($this->admin2, 'BCNA0001');
    $this->account22 = array(
      'exchange' => $this->exchange2,
      'account' => array(
        'name' => 'account22',
        'mail' => 'account22@integralces.net',
        'pass_raw' => 'integralces',
        'firstname' => 'Account22',
        'surname' => 'Noether',
        'address' => 'Gran vía 25',
        'town' => 'Barcelona',
        'postcode' => '08015',
        'phonemobile' => '333333333',
        'phonework' => '444444444',
        'phonehome' => '',
        'website' => 'https://integralces.net',
      ),
      'bankaccount' => array(
        'kind' => '0',
      ),
      'offer' => array(),
      'want' => array(),
    );
    $this->createAccount($this->account22);
    $this->activateAccount($this->admin2, 'BCNA0002');
  }
  /**
   * Do several transactions.
   */
  protected function doTransactions() {
    $user1 = $this->accountUser($this->account11);
    $user2 = $this->accountUser($this->account12);
    $record = array(
      'buyer' => 'HORA0002',
      'concept' => 'First test transaction.',
      'amount' => '1.25',
    );
    $this->doTransaction($user1, $record);
    $this->assertBalance($user1, '1.25');
    $this->assertBalance($user2, '-1.25');
    $record = array(
      'buyer' => 'HORA0002',
      'concept' => 'Second test transaction.',
      'amount' => '0.75',
    );
    $this->doTransaction($user1, $record);
    $this->assertBalance($user1, '2');
    $this->assertBalance($user2, '-2');
    $record = array(
      'buyer' => 'HORA0001',
      'concept' => 'Third test transaction.',
      'amount' => '0.5',
    );
    $this->doTransaction($user2, $record);
    $this->assertBalance($user1, '1.5');
    $this->assertBalance($user2, '-1.5');
  }
  /**
   * Do several transactions.
   */
  protected function doTransactionsInterexchange() {
    // Create virtual accounts from interexchange.
    $user1 = $this->accountUser($this->account11);
    $user2 = $this->accountUser($this->account21);
    // Interexchange.
    $record = array(
      'seller' => 'HORA0001',
      'buyer' => 'BCNA0001',
      'concept' => 'Fourth test transaction.',
      'amount' => '0.5',
    );
    $this->doTransaction($user1, $record, TRUE, 'The transaction has NOT been applied');
    // Activate virtual accounts from interexchange.
    $this->drupalLogin($this->admin1);
    $this->drupalGet('ces/admin/account');
    $this->clickLink('HORABCNA');
    $this->drupalPost(NULL, array('state' => 1), t('Save'));
    $this->drupalLogout();
    $this->drupalLogin($this->admin2);
    $this->drupalGet('ces/admin/account');
    $this->clickLink('BCNAHORA');
    $this->drupalPost(NULL, array('state' => 1), t('Save'));
    $this->drupalLogout();
    // Confirm transaction.
    $this->drupalLogin($user1);
    $this->drupalGet('ces/bank/account/transaction/8/edit');
    $this->drupalPost(NULL, NULL, t('Update transaction'));
    $this->drupalPost(NULL, NULL, t('Update transaction'));
    $this->drupalLogout();
    $this->assertBalance($user1, '10.75');
    $this->assertBalance($user2, '-5');
  }
  /**
   * Do an interexchange transaction with manual accept payment.
   */
  protected function checkInterexchangeManualAccept() {
    $user1 = $this->accountUser($this->account11);
    $user2 = $this->accountUser($this->account21);
    $transaction = array(
      'seller' => 'BCNA0001',
      'buyer' => 'HORA0001',
      'concept' => 'Test manual authorization with interexchange transaction.',
      'amount' => '1',
    );
    $this->doTransaction($user2, $transaction, FALSE, t('The transaction must be manually authorized.'));
    // Check tansaction state from both sides.
    $this->checkTransactionState($user1, $transaction, t('Awaiting acceptance'));
    $this->checkTransactionState($user2, $transaction, t('Awaiting acceptance'));
    $this->acceptTransaction($user1, $transaction);
    $this->checkTransactionState($user1, $transaction, t('Committed'));
    $this->checkTransactionState($user2, $transaction, t('Committed'));
    $this->assertBalance($user2, '-4');
    $this->assertBalance($user1, '11.15');
  }
  /**
   * Check the state of a given transaction.
   */
  protected function checkTransactionState($user, $transaction, $state) {
    $this->drupalLogin($user);
    $this->drupalGet('ces/bank/account/transaction');
    $this->clickLink($transaction['concept']);
    $this->assertText($state);
  }
  /**
   * Check administrative interface.
   */
  protected function adminTransactions() {
    $this->drupalLogin($this->admin1);
    $this->drupalGet('ces/admin/transaction');
    $this->assertText('First test transaction.');
    $this->assertText('Second test transaction.');
    $this->assertText('Third test transaction.');
    $this->assertText('Fourth test transaction.');
  }
  /**
   * Do transactions as admin.
   */
  protected function doAdminTransaction() {
    $this->drupalLogin($this->admin1);
    $this->drupalGet('ces/admin/transaction/new');
    $edit = array(
      'fromaccountname' => 'HORA0002',
      'toaccountname' => 'HORA0001',
      'concept' => 'Transaction from admin',
      'amount' => '0.5',
    );
    $this->drupalPost(NULL, $edit, t('Create transaction'));
    $this->drupalPost(NULL, array(), t('Confirm transaction'));
    $this->assertText('Transaction successfully applied.');
    $user1 = $this->accountUser($this->account11);
    $this->assertBalance($user1, '11.25');
    $user2 = $this->accountUser($this->account12);
    $this->assertBalance($user2, '-10.75');
  }
  /**
   * Helper function.
   *
   * @param object $record
   *   A user object suitable for drupalLogin() function.
   */
  protected function accountUser($record) {
    $user = (object) array(
      'name' => $record['account']['name'],
      'pass_raw' => $record['account']['pass_raw'],
      'mail' => $record['account']['mail'],
    );
    return $user;
  }
  /**
   * Helper function.
   *
   * @param array $record
   *   User account form array with proper keys.
   */
  protected function accountRecord($record) {
    $ret = array(
      'name' => $record['name'],
      'mail' => $record['mail'],
      'pass[pass1]' => $record['pass_raw'],
      'pass[pass2]' => $record['pass_raw'],
      'ces_firstname[und][0][value]' => $record['firstname'],
      'ces_surname[und][0][value]' => $record['surname'],
      'ces_address[und][0][value]' => $record['address'],
      'ces_town[und][0][value]' => $record['town'],
      'ces_postcode[und][0][value]' => $record['postcode'],
      'ces_phonemobile[und][0][value]' => $record['phonemobile'],
      'ces_phonework[und][0][value]' => $record['phonework'],
      'ces_phonehome[und][0][value]' => $record['phonehome'],
      'ces_website[und][0][value]' => $record['website'],
    );
    return $ret;
  }
  /**
   * Create a new user request.
   *
   * @param array $record
   *   Associative array with entries:
   *   - exchange: associative array with entries
   *     - country
   *     - fullname
   *   - account: associative array with entries:
   *     - name
   *     - mail
   *     - firstname
   *     - surname
   *     - address
   *     - town
   *     - postcode
   *     - phonemobile
   *     - phonework
   *     - phonehome
   *     - website
   *   - bankaccount: associative array with entries:
   *     - kind: type of account. Options are 0 (Individual), 1 (Shared),
   *             2 (Organization), 3 (Company), 4 (Public).
   *   - offer: array with zero or more entries. Each of them is an associative
   *   array with entries:
   *     - title
   *     - body
   *     - rate
   *     - keywords
   *   - want: array with zero or more entries. Each of them is an associative
   *   array with entries:
   *     - title
   *     - body
   *     - keywords
   *   .
   */
  protected function createAccount($record) {
    // Choose country from first page.
    $this->drupalGet('');
    $country = array(
      'country' => $record['exchange']['country'],
    );
    $this->drupalPost(NULL, $country, t('Register'));
    // Choose exchange.
    $this->clickLink($record['exchange']['fullname']);
    // User account. Change field names.
    $account = $this->accountRecord($record['account']);
    $this->drupalPost(NULL, $account, t('Next >>'));
    // CesBank account, offers and wants.
    $offers = 0;
    $wants = 0;
    $edit = $record['bankaccount'];
    while ($edit !== FALSE) {
      if ($offers < count($record['offer'])) {
        $op = t('Add offer >>');
        $this->drupalPost(NULL, $edit, $op);
        // Offer.
        $offer = $record['offer'][$offers];
        $edit = array();
        $edit['offers[' . $offers . '][title]'] = $offer['title'];
        $edit['offers[' . $offers . '][body]'] = $offer['body'];
        $edit['offers[' . $offers . '][ces_offer_rate][und][0][value]'] = $offer['rate'];
        $edit['offers[' . $offers . '][keywords]'] = $offer['keywords'];
        $offers++;
      }
      elseif ($wants < count($record['want'])) {
        $op = t('Add want >>');
        $this->drupalPost(NULL, $edit, $op);
        // Want.
        $want = $record['want'][$wants];
        $edit = array();
        $edit['wants[' . $wants . '][title]'] = $want['title'];
        $edit['wants[' . $wants . '][body]'] = $want['body'];
        $edit['wants[' . $wants . '][keywords]'] = $want['keywords'];
        $wants++;
      }
      else {
        $op = t('Submit registration');
        $this->drupalPost(NULL, $edit, $op);
        $edit = FALSE;
      }
    }
    $this->assertText(t('See you soon!'));
  }
  /**
   * Activate a requested account.
   *
   * @param object $admin
   *   The exchange admin user.
   * @param staring $account_name
   *   The Account name.
   */
  protected function activateAccount($admin, $account_name) {
    // Login exchange admin.
    $this->drupalLogin($admin);
    // Go to accounts admin page.
    $this->drupalGet('ces/admin/account');
    $this->assertText(t('Hid.'));
    $this->clickLink($account_name);
    $this->drupalPost(NULL, array('state' => '1'), t('Save'));
    $this->drupalLogout();
  }
  /**
   * Create a new exchange request.
   */
  protected function createExchange($record) {
    $this->drupalGet('');
    $this->clickLink(t('New exchange'));
    $this->assertText(t('General'));
    $this->assertText(t('Administrator'));
    $this->assertText(t('Location'));
    $this->assertText(t('Currency'));
    $this->assertText(t('Members'));
    $this->drupalPost(NULL, $record, t('Create exchange'));
    $this->assertRaw(t('Exchange %code successfully created.', array('%code' => $record['code'])));
  }
  /**
   * Activate created exchange.
   */
  protected function activateExchange($record) {
    $this->drupalLogin($this->globalAdminUser);
    $this->drupalGet('ces/admin/ces');
    $this->clickLink($record['code'] . ' - ' . $record['fullname']);
    $this->assertFieldByName('code', $record['code']);
    $this->assertFieldByName('fullname', $record['fullname']);
    $this->drupalPost(NULL, array(), t('Activate exchange'));
    $this->assertRaw(t('Exchange %code successfully activated.', array('%code' => $record['code'])));
    $this->assertText(t('Limits'));
    $this->drupalLogout();
  }
  /**
   * Create a transaction.
   *
   * @param object $user
   *   The user who performs the transaction.
   * @param array $record
   *   An associative array with entries:
   *   - buyer: buyer' account number.
   *   - concept
   *   - amount
   *   - seller: seller's account number. If omitted, the default user's account
   *             will be used.
   * @param bool $success
   *   Whether this transaction should be permitted by the system.
   * @param string $message
   *   Optional, a message to assert after doing the transaction.
   * @param string $op
   *   Optional, whether this is a new transaction ('new') or we are editing an
   *   existing transaction ('edit').
   */
  protected function doTransaction($user, $record, $success = TRUE, $message = NULL, $op = 'new') {
    $this->drupalLogin($user);
    $this->clickLink(t('My account'));
    if ($op == 'new') {
      $this->clickLink(t('Enter transaction'));
    }
    else {
      $this->clickLink(t('Account statement'));
      $this->clickLink($record['concept']);
      $this->clickLink(t('Edit'));
    }
    $edit = array(
      'fromaccountname' => $record['buyer'],
      'concept' => $record['concept'],
      'amount' => $record['amount'],
    );
    if (!empty($record['seller'])) {
      if (drupal_substr($record['seller'], 0, 4) != drupal_substr($record['buyer'], 0, 4)) {
        // Interexchange transaction.
        $name = drupal_substr($record['buyer'], 0, 4);
        $element = current($this->xpath($this->constructFieldXpath('name', 'fromexchange')));
        $options = $this->getAllOptions($element);
        foreach ($options as $option) {
          if (((string) $option) == $name) {
            $edit['fromexchange'] = ((string) $option['value']);
            $this->drupalPostAJAX(NULL, array('fromexchange' => $edit['fromexchange']), 'fromexchange');
            break;
          }
        }
      }
    }
    $button = ($op == 'new' ? t('Create transaction') : t('Update transaction'));
    $this->drupalPost(NULL, $edit, $button);
    $this->assertText($record['buyer']);
    if (isset($record['seller'])) {
      $this->assertText($record['seller']);
    }
    $this->assertText($record['amount']);
    $button = ($op == 'new' ? t('Confirm transaction') : t('Update transaction'));
    $this->drupalPost(NULL, array(), $button);
    if (!$message) {
      $message = ($op == 'new' ? t('Transaction successfully applied.') : t('Transaction successfully updated.'));
      if ($success) {
        $this->assertText($message);
      }
      else {
        $this->assertNoText($message);
      }
    }
    else {
      $this->assertRaw($message);
    }
    $this->drupalLogout();
  }
  /**
   * Remove a transaction.
   * */
  protected function removeTransaction($user, $record, $linkindex = 0) {
    $this->drupalLogin($user);
    $this->clickLink(t('My account'));
    $this->clickLink(t('Account statement'));
    $this->clickLink($record['concept'], $linkindex);
    $this->clickLink(t('Delete'));
    $this->assertText(t('Are you sure you want to delete the transaction?'));
    $this->assertText($record['concept']);
    $this->assertText($record['buyer']);
    $this->drupalPost(NULL, array(), t('Delete'));
    $this->assertText('has been deleted');
  }
  /**
   * Check that the given user has the given balance in his/her default account.
   */
  public function assertBalance($user, $amount) {
    $this->drupalLogin($user);
    $xpath = $this->xpath("//div[contains(@class, 'ces_summaryblock_number')]/div[contains(text(), 'Balance')]/../div[contains(@class, 'ces_summaryblock_value')]/text()");
    $balance = (string) ($xpath[0]);
    $this->assert(strpos($balance, $amount) !== FALSE, 'The amount balance is correct.');
    $this->assertText($amount);
    $this->drupalLogout();
  }
  /**
   * Test function for limited accounts.
   */
  protected function checkLimits() {
    $limitchain = array(
      'name' => 'test_limit',
      'limits' => array(
        array(
          'type' => 'debit',
          'value' => '-10',
          'block' => FALSE,
        ),
        array(
          'type' => 'debit',
          'value' => '-20',
          'block' => TRUE,
        ),
      ),
    );
    $this->createLimit($this->admin1, $limitchain);
    $this->setAccountLimit($this->admin1, 'HORA0002', 'test_limit');
    $user1 = $this->accountUser($this->account11);
    // Do transaction OK.
    $this->doTransaction($user1, array(
      'buyer' => 'HORA0002',
      'concept' => 'Limit test transaction OK.',
      'amount' => '8',
    ));
    // HORA0002: -9.5 (Inside limits).
    $this->doTransaction($user1, array(
      'buyer' => 'HORA0002',
      'concept' => 'Limit test transaction OK.',
      'amount' => '10',
    ));
    // HORA0002: -19.5 (Outside nonblocking limit, inside blocking limit).
    // TODO: Check effect of outside nonblocking limit!.
    $this->doTransaction($user1, array(
      'buyer' => 'HORA0002',
      'concept' => 'Limit test transaction OK.',
      'amount' => '2',
    ), FALSE);
    $user2 = $this->accountUser($this->account12);
    $this->assertBalance($user2, '-19.50');

    // TODO: check interexchenge KO.
  }
  /**
   * Check balance and sales visibility options.
   */
  protected function checkVisibilityOptions() {
    $user1 = $this->accountUser($this->account11);
    $user1 = user_load_by_name($user1->name);
    // Balance and sales are visible by default.
    $this->drupalLogin($this->accountUser($this->account12));
    $this->drupalGet('user/' . $user1->uid);
    // We use assertRaw with dt tag not to confuse these entries with the ones
    // present in summary block.
    $this->assertRaw('<dt>' . t('Balance') . '</dt>');
    $this->assertRaw('<dt>' . t('Sales') . '</dt>');
    // Change options.
    $this->drupalLogin($this->admin1);
    $this->drupalGet('ces/admin/exchange');
    $edit = array(
      'hide_balance' => TRUE,
      'hide_sales' => TRUE,
    );
    $this->drupalPost(NULL, $edit, t('Update exchange'));
    $this->drupalGet('user/' . $user1->uid);
    // Admin can anyway see the data.
    $this->assertRaw('<dt>' . t('Balance') . '</dt>');
    $this->assertRaw('<dt>' . t('Sales') . '</dt>');
    // But regular users can't.
    $this->drupalLogin($this->accountUser($this->account12));
    $this->drupalGet('user/' . $user1->uid);
    $this->assertNoRaw('<dt>' . t('Balance') . '</dt>');
    $this->assertNoRaw('<dt>' . t('Sales') . '</dt>');
  }
  /**
   * Create limit chain from administrative interface.
   *
   * @param object $admin
   *   The exchange administrator user.
   * @param array $limitchain
   *   Array describing the limit chain to be created, with the structure:
   *   - name: The name of the limit.
   *   - limits: Array of arrays with elements:
   *     - type: One of 'debit' or 'credit'.
   *     - value: The value of limit.
   *     - block: Whether the limit is blocking.
   */
  protected function createLimit($admin, $limitchain) {
    $this->drupalLogin($admin);
    $this->drupalGet('ces/admin/limitchain/new');
    // Create limits.
    foreach ($limitchain['limits'] as $limit) {
      $edit = array(
        'name' => $limitchain['name'],
        'newlimit[classname]' => $limit['type'] == 'credit' ? 'CesBankAbsolteCreditLimit' : 'CesBankAbsoluteDebitLimit',
      );
      $this->drupalPost(NULL, $edit, t('Add limit'));
    }
    // Setup limits.
    $count = 0;
    $edit = array('name' => $limitchain['name']);
    foreach ($limitchain['limits'] as $limit) {
      $edit['limits[' . $count . '][block]'] = $limit['block'];
      $edit['limits[' . $count . '][value]'] = $limit['value'];
      $count++;
    }
    $this->drupalPost(NULL, $edit, t('Create limit chain'));
    $this->assertRaw(t('Limit chain %name successfully created.',
      array('%name' => $limitchain['name'])));
  }
  /**
   * Set a limit chin to an account.
   */
  protected function setAccountLimit($admin, $accountname, $limitname) {
    // Go to account edit form.
    $this->drupalLogin($admin);
    $this->drupalGet('ces/admin/account');
    $this->clickLink('HORA0002');
    // Setup account.
    $limit = $this->xpath('//option[text()=:limitname]', array(':limitname' => $limitname));
    $edit = array('limitchain' => $limit[0]['value']);
    $this->drupalPost(NULL, $edit, t('Save'));
  }
  /**
   * Test manual payments authorization feature.
   */
  protected function checkManualAuthorization() {
    $user1 = $this->accountUser($this->account11);
    $user2 = $this->accountUser($this->account12);
    $this->setManualAuthorization($user1);
    // From now on, user11 will have manual authorization enabled.
    $transaction = array(
      'buyer' => 'HORA0001',
      'concept' => 'Test manual authorization.',
      'amount' => '9.25',
    );
    $this->doTransaction($user2, $transaction, FALSE, t('The transaction must be manually authorized.'));
    $this->assertBalance($user1, '19.5');
    $this->acceptTransaction($user1, $transaction);
    $this->assertBalance($user1, '10.25');
  }
  /**
   * Set manual authorization to user.
   */
  protected function setManualAuthorization($loginuser, $whitelist = array()) {
    $this->drupalLogin($loginuser);
    $user = user_load_by_name($loginuser->name);
    $this->drupalGet('user/' . $user->uid . '/edit');
    // Get checkbox name.
    $res = $this->xpath('//label[normalize-space(text())=:label]/..//input',
      array(':label' => t('Accept payments manually')));
    $manualname = (string) $res[0]['name'];
    // Get whitelist textarea name.
    $res = $this->xpath('//label[normalize-space(text())=:label]/..//textarea',
      array(':label' => t('Accept payments whitelist')));
    $whitelistname = (string) $res[0]['name'];
    $edit = array(
      $manualname => TRUE,
      $whitelistname => implode("\n", $whitelist),
    );
    $this->drupalPost(NULL, $edit, t('Save'));
  }
  /**
   * Manually accept transaction.
   */
  protected function acceptTransaction($user, $transaction) {
    $this->drupalLogin($user);
    $this->drupalGet('ces/bank/account/transaction');
    $this->clickLink($transaction['concept']);
    $this->clickLink(t('Accept/Reject'));
    $this->drupalPost(NULL, array(), t('Accept'));
    $this->assertText(t('Transaction successfully applied.'));
  }
  /**
   * Admin accounts.
   */
  protected function adminAccounts() {
    // CRUD accounts from admin.
    $this->drupalLogin($this->admin2);
    $this->drupalGet('ces/admin/account');
    $this->assertLink('BCNA0000');
    $this->assertLink('adminbcna');
    $this->assertText('test.admin');
    $this->clickLink('BCNA0000');
    $this->assertText('BCNA - Ecoxarxa de Barcelona');
    // Edited from admin.
    // The administrator (0000) should not be able to change the code.
    $this->drupalPost(NULL, array('name' => 'BCNA9001'), t('Save'));
    $this->assertText(t('account can not be changed'));
    // Have the four letters of the network.
    $this->drupalPost(NULL, array('name' => 'HOLA0000'), t('Save'));
    $this->assertText(t('The first four carecteres'));
    // Validate number of code.
    $this->drupalGet('ces/admin/account/5/edit');
    $this->drupalPost(NULL, array('name' => 'BCNA000A'), t('Save'));
    $this->assertText(t('The last four digits must be a number'));
    // Edited from admin another account.
    $this->drupalGet('ces/admin/account');
    $this->clickLink('BCNA0001');
    $this->drupalPost(NULL, array('name' => 'BCNA9001'), t('Save'));
    $this->assertText(t('Account successfully update'));
    // Add account users.
    $this->drupalGet('ces/admin/account');
    $this->clickLink('BCNA9001');
    $this->clickLink(t('List users'));
    $this->assertText(t('account21@integralces.net'));
    $this->clickLink(t('Add account users'));
    $this->drupalPost(NULL, array('username' => 'account22', 'privilege' => 1), t('Save'));
    // Delete.
    $this->drupalGet('ces/admin/account');
    $this->clickLink('BCNA9001');
    $this->clickLink(t('Delete'));
    $this->drupalPost(NULL, NULL, t('Close account'));
    $this->drupalPost(NULL, NULL, t('Confirm'));
    $this->assertText(t('The account BCNA9001 has been closed'));
    // Create new user+account.
    $this->drupalGet('ces/admin/account/newuser');
    $this->drupalPost(NULL, array(
      'name' => 'testuser',
      'mail' => 'testuser@test.com',
      'pass[pass1]' => 'passwd',
      'pass[pass2]' => 'passwd',
      'ces_firstname[und][0][value]' => 'firstname',
      'ces_town[und][0][value]' => 'town',
      'ces_postcode[und][0][value]' => 'postcode',
      ),
      t('Create new account')
    );
    $this->assertText(t('Created a new user account for'));
    $this->assertText(t('BCNA0003'));
    $this->assertLink(t('testuser'));
    $this->assertText(t('testuser@t'));
    // Create new account related to an user of another network.
    $this->drupalGet('ces/admin/account/new');
    $this->drupalPost(NULL, array('users[0][username]' => 'account12'), t('Save'));
    $this->assertText(t('Account successfully created.'));
    $this->drupalLogout();
    // Transactions of an account.
    $this->drupalLogin($this->admin1);
    $this->drupalGet('ces/admin/account');
    $this->clickLink('HORA0002');
    $this->clickLink(t('Transactions'));
    $this->assertText('HORA0002');
    $this->assertText('HORA0001');
    $this->assertLink(t('Second test transaction.'));
    $this->drupalLogout();
  }
  /**
   * Check the other echanges page.
   */
  protected function checkOtherAccountsPage() {
    $this->drupalLogin($this->accountUser($this->account11));
    $this->drupalGet('ces/bank/exchange/accounts/otherexchanges');
    $this->assertText('BCNA - Ecoxarxa de Barcelona');
    $this->assertText('ecos (10.00ECO = 1.00ℏ)');
    $this->assertText('0.10');
    $this->assertText('0.50');
    $this->assertText('+0.40');
    $this->drupalLogout();
  }
  /**
   * Test edit local transaction.
   */
  protected function doEditTransactions() {
    $user1 = $this->accountUser($this->account11);
    $user2 = $this->accountUser($this->account12);
    $record = array(
      'buyer' => 'HORA0002',
      'concept' => 'First test transaction.',
      'amount' => '1.50',
    );
    $this->doTransaction($user1, $record, TRUE, NULL, 'edit');
    $this->assertBalance($user1, '11.40');
    $this->assertBalance($user2, '-11.00');
  }
  /**
   * Test edit interexchange transactions.
   */
  protected function doEditInterexchangeTransactions() {
    $user1 = $this->accountUser($this->account11);
    $user2 = $this->accountUser($this->account21);
    // Interexchange.
    $record = array(
      'seller' => 'HORA0001',
      'buyer' => 'BCNA0001',
      'concept' => 'Fourth test transaction.',
      'amount' => '1.5',
    );
    $this->doTransaction($user1, $record, TRUE, NULL, 'edit');
    $this->assertBalance($user1, '12.40');
    $this->assertBalance($user2, '-14.00');
  }
  /**
   * Test remove local transaction.
   * **/
  protected function doRemoveTransactions() {
      $user1 = $this->accountUser($this->account11);
      $user2 = $this->accountUser($this->account12);
      $record = array(
        'buyer' => 'HORA0002',
        'concept' => 'First test transaction.',
      );
      $this->removeTransaction($user1, $record, 1);
      $this->assertBalance($user1, '9.40');
      $this->assertBalance($user2, '-9.50');
  }
  /**
   * Test remove interexchange transaction.
   * **/
   protected function doRemoveInterexchangeTransactions() {
      $user1 = $this->accountUser($this->account11);
      $user2 = $this->accountUser($this->account21);
      $record = array(
        'buyer' => 'BCNA0001',
        'concept' => 'Fourth test transaction.',
      );
      $this->removeTransaction($user1, $record, 1);
      $this->assertBalance($user1, '10.90');
      $this->assertBalance($user2, '1.00');
  }
}
/**
 * @}
 */
