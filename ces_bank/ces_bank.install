<?php
/**
 * @file
 * Install, update and uninstall functions for the ces bank module.
 */

/**
 * @defgroup ces_bank_install Install Ces Bank
 * @ingroup ces_bank
 * @{
 * Install, update and uninstall functions for the ces bank module.
 */

/**
 * Implements hook_install().
 */
function ces_bank_install() {
  $t = get_t();
  $ok3 = _ces_bank_create_default_permissions();
  if (!$ok3) {
    drupal_set_message($t('Error creating default permissions.'), 'error');
  }
  if ($ok3) {
    drupal_set_message($t('Initial records created.'));
  }
}
/**
 * Create permissions that, by default, all people will have.
 *
 * These include:
 *  - See global statistics
 *  - Create a new exchange
 */
function _ces_bank_create_default_permissions() {
  $record = array(
    'permission' => CesBankPermission::PERMISSION_VIEW,
    'object' => 'global statistics',
    'objectid' => 0,
    'scope' => 'global',
    'scopeid' => 0,
  );
  $ok = (drupal_write_record('ces_permission', $record) !== FALSE);
  $record = array(
    'permission' => CesBankPermission::PERMISSION_USE,
    'object' => 'global exchangecreator',
    'objectid' => 0,
    'scope' => 'global',
    'scopeid' => 0,
  );
  $ok = (drupal_write_record('ces_permission', $record) !== FALSE) && $ok;
  return $ok;
}

/**
 * Implements hook_enable().
 */
function ces_bank_enable() {
}
/**
 * Implements hook_schema().
 */
function ces_bank_schema() {
  $schema = array();
  $schema['ces_exchange'] = array(
    'description' => '',
    'fields' => array(
      'id' => array(
        'description' => 'ID of this exchange',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uuid_currency' => array(
        'type' => 'varchar',
        'length' => '36',
        'not null' => TRUE
      ),
      'code' => array(
        'description' => '4 letter id for the exchange',
        'type' => 'varchar',
        'length' => '4',
        'not null' => TRUE,
        'default' => '',
      ),
      'state' => array(
        'description' => 'The state of this exchange',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'shortname' => array(
        'description' => 'Exchange short name',
        'type' => 'varchar',
        'length' => '50',
        'not null' => TRUE,
        'default' => '',
      ),
      'name' => array(
        'description' => 'Exchange name',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'country' => array(
        'description' => 'Exchange country',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'region' => array(
        'description' => 'Exchange region',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'town' => array(
        'description' => 'Exchange town',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'website' => array(
        'description' => 'Exchange website',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'map' => array(
        'description' => 'Exchange map',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'admin' => array(
        'description' => 'UID of the exchange administrator.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'limitchain' => array(
        'description' => 'ID of the default limit chain for this exchange.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'currencysymbol' => array(
        'type' => 'varchar',
        'length' => '7',
        'not null' => TRUE,
        'default' => '',
      ),
      'currencyname' => array(
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'currenciesname' => array(
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'currencyvalue' => array(
        'description' => 'Value of this currency.',
        'type' => 'numeric',
        'precision' => 31,
        'scale' => 4,
        'not null' => TRUE,
        'default' => 1,
      ),
      'currencyscale' => array(
        'description' => 'How many decimals use for this currency. Between 0 and 4.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'modified' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'PHP-serialized exchange configuration.',
        'type' => 'blob',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
      'lat' => array(
        'description' => 'Latitude',
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0.0,
        'scale' => 10,
        'precision' => 15,
      ),
      'lng' => array(
        'description' => 'Longitude',
        'type' => 'numeric',
        'not null' => TRUE,
        'default' => 0.0,
        'scale' => 10,
        'precision' => 15,
      )
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'code' => array('code'),
      'ces_exchange_unique_uuid_currency' => array('uuid_currency')
    ),
    'foreign keys' => array(
      'limitchain' => array(
        'table' => 'ces_limitchain',
        'columns' => array('limitchain' => 'id'),
      ),
      'admin' => array(
        'table' => 'user',
        'columns' => array('admin' => 'uid'),
      ),
    ),
  );
  $schema['ces_limit'] = array(
    'description' => 'Acount limit objects',
    'fields' => array(
      'id' => array(
        'description' => 'ID of this account limit',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'limitchain' => array(
        'description' => 'Chain limit ID where this limit belongs.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'classname' => array(
        'description' => 'Limit type. The name of the class implementing it.',
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'value' => array(
        'description' => 'A value from which the account is in warning',
        'type' => 'numeric',
        'precision' => 31,
        'scale' => 4,
        'not null' => TRUE,
        'default' => 0,
      ),
      'block' => array(
        'description' => 'Whether to block transaction when the limit is reached.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'weight' => array(
        'description' => 'Wheight of this limit within the chain.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'PHP-serialized type-specific options.',
        'type' => 'blob',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'limitchain' => array(
        'table' => 'ces_limitchain',
        'columns' => array('limitchain' => 'id'),
      ),
    ),
  );
  $schema['ces_limitchain'] = array(
    'description' => 'Chain of account limits.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'exchange' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A full description of this limit or to which accounts is it applied.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'exchange' => array(
        'table' => 'ces_exchange',
        'columns' => array('exchange' => 'id'),
      ),
    ),
  );
  $schema['ces_account'] = array(
    'description' => 'Exchange account',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uuid' => array(
        'type' => 'varchar',
        'length' => '36',
        'not null' => TRUE
      ),
      'exchange' => array(
        'description' => 'To which exchange belongs this account',
        'type' => 'int',
        'unsigned' => TRUE,
        'notnull' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'description' => 'Public Account ID. It is 4-letter exchange code + account number',
        'type' => 'varchar',
        'length' => '31',
        'notnull' => TRUE,
        'default' => '',
      ),
      'balance' => array(
        'description' => 'Account balance.',
        'type' => 'numeric',
        'precision' => 31,
        'scale' => 4,
        'not null' => TRUE,
        'default' => 0,
      ),
      'limitchain' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'state' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'modified' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'kind' => array(
        'description' => 'One of INDIVIDUAL (0), SHARED (1), ORGANIZATION (2), COMPANY (3), PUBLIC (4)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'PHP-serialized account configuration.',
        'type' => 'blob',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'name' => array('name'),
      'ces_account_unique_uuid' => array('uuid'),
    ),
    'foreign keys' => array(
      'exchange' => array(
        'table' => 'ces_exchange',
        'columns' => array('exchange', 'id'),
      ),
      'limitchain' => array(
        'table' => 'ces_limitchain',
        'columns' => array('limitchain', 'id'),
      ),
    ),
  );
  $schema['ces_accountuser'] = array(
    'description' => 'Relation n by n between users and accounts.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'user' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'account' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'privilege' => array(
        'description' => 'Type of user relation with this account. 0 => user is owner.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'accountuser' => array('user', 'account'),
    ),
    'foreign keys' => array(
      'user' => array(
        'table' => 'user',
        'columns' => array('user', 'uid'),
      ),
      'account' => array(
        'table' => 'ces_account',
        'colums' => array('account', 'id'),
      ),
    ),
  );
  $schema['ces_transaction'] = array(
    'description' => 'Account transaction',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uuid' => array (
        'type' => 'varchar',
        'length' => '36',
        'not null' => TRUE
      ),
      'fromaccount' => array(
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'toaccount' => array(
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'amount' => array(
        'type' => 'numeric',
        'precision' => 31,
        'scale' => 4,
        'not null' => TRUE,
        'default' => 0,
      ),
      'user' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'concept' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'state' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'modified' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'PHP-serialized transaction extra data.',
        'type' => 'blob',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
      'decoration' => array(
        'description' => 'PHP-serialized transaction options.',
        'type' => 'blob',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(
      'fromaccount' => array(
        'table' => 'ces_account',
        'columns' => array('fromaccount' => 'name'),
      ),
      'toaccount' => array(
        'table' => 'ces_account',
        'columns' => array('toaccount' => 'name'),
      ),
      'user' => array(
        'table' => 'user',
        'columns' => array('user' => 'uid'),
      ),
    ),
    'unique keys' => array(
      'ces_transaction_unique_uuid' => array('uuid'),
    ),
    'indexes' => array(
      'ces_transaction_fromaccount' => array('fromaccount'),
      'ces_transaction_toaccount' => array('toaccount'),
    )
  );
  $schema['ces_permission'] = array(
    'description' => 'Permissions',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'permission' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'view=10, use=20, edit=30, admin=40',
      ),
      'object' => array(
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'objectid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'scope' => array(
        'type' => 'varchar',
        'length' => '31',
        'not null' => TRUE,
        'default' => '',
      ),
      'scopeid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'foreign keys' => array(),
  );
  return $schema;
}
/**
 * Implements hook_uninstall().
 */
function ces_bank_uninstall() {
  drupal_uninstall_schema(ces_bank_schema());
}

/**
 * Add new data field in accunt table.
 */
function ces_bank_update_7100() {
  $schema = ces_bank_schema();
  db_add_field('ces_account', 'data', $schema['ces_account']['data']);
}

/**
 * @param $table string Table name.
 * @param $field string UUID field name
 * @param $index boolean Add unique index?
 */
function _add_uuid_field($table, $field, $index) {
  $schema = ces_bank_schema();
  $spec = $schema[$table]['fields'][$field];
  $spec['initial'] = '00000000-0000-0000-0000-000000000000';
  db_add_field($table, $field, $spec);
  // Populate uuid field
  db_update($table)
    ->expression($field, '(SELECT UUID())')
    ->execute();
  if ($index) {
    db_add_unique_key($table, $table . '_unique_' . $field, array($field));
  }
}

/**
 * Add uuid field to transactions (needed by komunitin).
 */
function ces_bank_update_7101() {
  _add_uuid_field('ces_transaction', 'uuid', TRUE);
}

/**
 * Add uuid field to accounts (needed by komunitin).
 */
function ces_bank_update_7102() {
  _add_uuid_field('ces_account', 'uuid', TRUE);
}

/**
 * Add uuid fields to exchanges (needed by komunitin).
 */
function ces_bank_update_7103() {
  // uuid as a komunitin currency.
  _add_uuid_field('ces_exchange', 'uuid_currency', TRUE);
}

/**
 * Add geolocation fields.
 */
function ces_bank_update_7104() {
  // uuid as a komunitin currency.
  $schema = ces_bank_schema();
  $lat = $schema['ces_exchange']['fields']['lat'];
  db_add_field('ces_exchange', 'lat', $lat);
  $lng = $schema['ces_exchange']['fields']['lng'];
  db_add_field('ces_exchange', 'lng', $lng);
}

/**
 * Add account index to transaction table.
 */
function ces_bank_update_7105() {
  db_add_index('ces_transaction', 'ces_transaction_fromaccount', ['fromaccount']);
  db_add_index('ces_transaction', 'ces_transaction_toaccount', ['toaccount']);
}

/** @} */
